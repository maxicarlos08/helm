{{/* Expand the name of the chart. */}}
{{- define "torvus.name" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/* Create a default fully qualified app name. truncated to 63 chars for k8s DNS, dont duplicate with chart name */}}
{{- define "torvus.fullname" -}}
{{- if .Values.fullnameOverride }}
{{- .Values.fullnameOverride | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- $name := default .Chart.Name .Values.nameOverride }}
{{- if contains $name .Release.Name }}
{{- .Release.Name | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" }}
{{- end }}
{{- end }}
{{- end }}


{{/* torvus Resource Names */}}

{{- define "torvus.deployment" -}}
{{- (printf "%s" (include "torvus.fullname" .)) }}
{{- end }}

{{- define "torvus.deploymentKind" -}}
Deployment
{{- end }}

{{- define "torvus.serviceAccount" -}}
{{- (printf "%s-serviceaccount" (include "torvus.fullname" .)) }}
{{- end }}

{{- define "torvus.configmap" -}}
{{- (printf "%s-configmap" (include "torvus.fullname" .)) }}
{{- end }}

{{- define "torvus.configMapGrafana" -}}
{{- (printf "%s-dashboards" (include "torvus.fullname" .)) }}
{{- end }}

{{- define "torvus.secret" -}}
{{- (printf "%s-secret" (include "torvus.fullname" .)) }}
{{- end }}

{{- define "torvus.podMonitor" -}}
{{- (printf "%s-podmonitor" (include "torvus.fullname" .)) }}
{{- end }}

{{/* Common labels */}}
{{- define "torvus.labels" -}}
{{ include "torvus.selectorLabels" . }}
{{- if .Chart.AppVersion }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
{{- end }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- end }}

{{/* Selector labels */}}
{{- define "torvus.selectorLabels" -}}
app.kubernetes.io/name: {{ include "torvus.deployment" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- end }}