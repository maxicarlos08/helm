apiVersion: apps/v1
kind: {{ include "moderation-tool.deploymentKind" . }}
metadata:
  name: {{ include "moderation-tool.deployment" . }}
  labels:
    {{- include "moderation-tool.labels" . | nindent 4 }}
spec:
  replicas: {{ .Values.replicaCount }}
  {{if .Values.persistence.enabled }}serviceName: {{ include "moderation-tool.service" . }}{{ end }}
  selector:
    matchLabels:
      {{- include "moderation-tool.selectorLabels" . | nindent 6 }}
  template:
    metadata:
      annotations:
        {{ if and .Values.persistence.enabled .Values.persistence.backup }}backup.velero.io/backup-volumes: {{ include "moderation-tool.volumeClaim" . | quote }}{{ end }}
      {{- with .Values.podAnnotations }}
        {{- toYaml . | nindent 8 }}
      {{- end }}
      labels:
        app: "moderation-tool"
        loki: "moderation-tool"
        {{- include "moderation-tool.selectorLabels" . | nindent 8 }}
    spec:
      {{- with .Values.image.pullSecrets }}
      imagePullSecrets:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      serviceAccountName: {{ include "moderation-tool.serviceAccount" . }}
      securityContext:
        {{- toYaml .Values.podSecurityContext | nindent 8 }}
      containers:
        - name: "auth"
          securityContext:
            {{- toYaml .Values.securityContext | nindent 12 }}
          image: "{{ .Values.image.repository }}:{{ .Values.image.tag | default .Chart.AppVersion }}"
          imagePullPolicy: {{ .Values.image.pullPolicy }}
          envFrom:
            - configMapRef:
                name: {{ include "moderation-tool.configMapSettings" . }}
          env:
            - name: CHAT_TOKEN
              valueFrom:
                secretKeyRef:
                  name: {{ include "moderation-tool.secret" . }}
                  key: chat-token
            - name: MODERATION_ENDPOINT_TOKEN
              valueFrom:
                secretKeyRef:
                  name: {{ include "moderation-tool.secret" . }}
                  key: moderation-endpoint-token
            - name: DATABASE
              valueFrom:
                secretKeyRef:
                  name: {{ include "moderation-tool.secret" . }}
                  key: database
          ports:
            - name: http
              containerPort: 8080
              protocol: TCP
          livenessProbe:
            httpGet:
              path: /health
              port: http
            periodSeconds: 10
            failureThreshold: 5
          readinessProbe:
            httpGet:
              path: /health
              port: http
            periodSeconds: 10
            failureThreshold: 5
          volumeMounts:
            {{if .Values.persistence.enabled -}}
            - name: {{ include "moderation-tool.volumeClaim" . }}
              mountPath: /opt/app/data
            {{ else }}
            - name: emptysaves
              mountPath: /opt/app/data
            {{- end }}
          resources:
            {{- toYaml .Values.resources | nindent 12 }}
      {{if not .Values.persistence.enabled -}}
      volumes:
        - name: emptysaves
          emptyDir: {}
      {{- end }}
      {{- with .Values.nodeSelector }}
      nodeSelector:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      {{- with .Values.affinity }}
      affinity:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      {{- with .Values.tolerations }}
      tolerations:
        {{- toYaml . | nindent 8 }}
      {{- end }}
  {{if .Values.persistence.enabled -}}
  volumeClaimTemplates:
    - metadata:
        name: {{ include "moderation-tool.volumeClaim" . }}
        annotations:
        {{- with .Values.persistence.annotations }}
          {{- toYaml . | nindent 10 }}
        {{- end }}
      spec:
        accessModes: [ "ReadWriteOnce" ]
        storageClassName: {{ .Values.persistence.storageClass }}
        resources:
          requests:
            storage: {{ .Values.persistence.size }}
  {{- end }}