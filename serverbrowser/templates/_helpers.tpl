{{/* Expand the name of the chart. */}}
{{- define "serverbrowser.name" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/* Create a default fully qualified app name. truncated to 63 chars for k8s DNS, dont duplicate with chart name */}}
{{- define "serverbrowser.fullname" -}}
{{- if .Values.fullnameOverride }}
{{- .Values.fullnameOverride | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- $name := default .Chart.Name .Values.nameOverride }}
{{- if contains $name .Release.Name }}
{{- .Release.Name | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" }}
{{- end }}
{{- end }}
{{- end }}


{{/* serverbrowser Resource Names */}}

{{- define "serverbrowser.service" -}}
{{- (printf "%s" (include "serverbrowser.fullname" .)) }}
{{- end }}

{{- define "serverbrowser.deployment" -}}
{{- (printf "%s" (include "serverbrowser.fullname" .)) }}
{{- end }}

{{- define "serverbrowser.deploymentKind" -}}
Deployment
{{- end }}

{{- define "serverbrowser.serviceAccount" -}}
{{- (printf "%s-serviceaccount" (include "serverbrowser.fullname" .)) }}
{{- end }}

{{- define "serverbrowser.secret" -}}
{{- (printf "%s-secret" (include "serverbrowser.fullname" .)) }}
{{- end }}

{{- define "serverbrowser.ingressSecret" -}}
{{- (printf "%s-ingresssecret" (include "serverbrowser.fullname" .)) }}
{{- end }}

{{- define "serverbrowser.ingress" -}}
{{- (printf "%s-ingress" (include "serverbrowser.fullname" .)) }}
{{- end }}

{{/* Common labels */}}
{{- define "serverbrowser.labels" -}}
{{ include "serverbrowser.selectorLabels" . }}
{{- if .Chart.AppVersion }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
{{- end }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- end }}

{{/* Selector labels */}}
{{- define "serverbrowser.selectorLabels" -}}
app.kubernetes.io/name: {{ include "serverbrowser.deployment" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- end }}