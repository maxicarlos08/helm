{{/* Expand the name of the chart. */}}
{{- define "labbot.name" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/* Create a default fully qualified app name. truncated to 63 chars for k8s DNS, dont duplicate with chart name */}}
{{- define "labbot.fullname" -}}
{{- if .Values.fullnameOverride }}
{{- .Values.fullnameOverride | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- $name := default .Chart.Name .Values.nameOverride }}
{{- if contains $name .Release.Name }}
{{- .Release.Name | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" }}
{{- end }}
{{- end }}
{{- end }}


{{/* labbot Resource Names */}}

{{- define "labbot.deployment" -}}
{{- (printf "%s" (include "labbot.fullname" .)) }}
{{- end }}

{{- define "labbot.deploymentKind" -}}
Deployment
{{- end }}

{{- define "labbot.serviceAccount" -}}
{{- (printf "%s-serviceaccount" (include "labbot.fullname" .)) }}
{{- end }}

{{- define "labbot.secret" -}}
{{- (printf "%s-secret" (include "labbot.fullname" .)) }}
{{- end }}

{{/* Common labels */}}
{{- define "labbot.labels" -}}
{{ include "labbot.selectorLabels" . }}
{{- if .Chart.AppVersion }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
{{- end }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- end }}

{{/* Selector labels */}}
{{- define "labbot.selectorLabels" -}}
app.kubernetes.io/name: {{ include "labbot.deployment" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- end }}