{{/* Expand the name of the chart. */}}
{{- define "auth.name" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/* Create a default fully qualified app name. truncated to 63 chars for k8s DNS, dont duplicate with chart name */}}
{{- define "auth.fullname" -}}
{{- if .Values.fullnameOverride }}
{{- .Values.fullnameOverride | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- $name := default .Chart.Name .Values.nameOverride }}
{{- if contains $name .Release.Name }}
{{- .Release.Name | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" }}
{{- end }}
{{- end }}
{{- end }}


{{/* auth Resource Names */}}

{{- define "auth.service" -}}
{{- (printf "%s" (include "auth.fullname" .)) }}
{{- end }}

{{- define "auth.deployment" -}}
{{- (printf "%s" (include "auth.fullname" .)) }}
{{- end }}

{{- define "auth.deploymentKind" -}}
{{- if .Values.persistence.enabled -}}StatefulSet{{- else -}}Deployment{{- end }}
{{- end }}

{{- define "auth.volumeClaim" -}}
{{- (printf "%s-data" (include "auth.fullname" .)) }}
{{- end }}

{{- define "auth.serviceAccount" -}}
{{- (printf "%s-serviceaccount" (include "auth.fullname" .)) }}
{{- end }}

{{- define "auth.configMapSettings" -}}
{{- (printf "%s-settings" (include "auth.fullname" .)) }}
{{- end }}

{{- define "auth.secret" -}}
{{- (printf "%s-secret" (include "auth.fullname" .)) }}
{{- end }}

{{- define "auth.ingress" -}}
{{- (printf "%s-ingress" (include "auth.fullname" .)) }}
{{- end }}

{{- define "auth.ingressSecret" -}}
{{- (printf "%s-ingresssecret" (include "auth.fullname" .)) }}
{{- end }}

{{- define "auth.serviceMonitor" -}}
{{- (printf "%s-servicemonitor" (include "auth.fullname" .)) }}
{{- end }}

{{/* Common labels */}}
{{- define "auth.labels" -}}
{{ include "auth.selectorLabels" . }}
{{- if .Chart.AppVersion }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
{{- end }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- end }}

{{/* Selector labels */}}
{{- define "auth.selectorLabels" -}}
app.kubernetes.io/name: {{ include "auth.deployment" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- end }}