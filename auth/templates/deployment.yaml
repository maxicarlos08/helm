apiVersion: apps/v1
kind: {{ include "auth.deploymentKind" . }}
metadata:
  name: {{ include "auth.deployment" . }}
  labels:
    {{- include "auth.labels" . | nindent 4 }}
spec:
  replicas: {{ .Values.replicaCount }}
  {{if .Values.persistence.enabled }}serviceName: {{ include "auth.service" . }}{{ end }}
  selector:
    matchLabels:
      {{- include "auth.selectorLabels" . | nindent 6 }}
  template:
    metadata:
      annotations:
        {{ if and .Values.persistence.enabled .Values.persistence.backup }}backup.velero.io/backup-volumes: {{ include "auth.volumeClaim" . | quote }}{{ end }}
      {{- with .Values.podAnnotations }}
        {{- toYaml . | nindent 8 }}
      {{- end }}
      labels:
        app: "auth"
        loki: "auth"
        {{- include "auth.selectorLabels" . | nindent 8 }}
    spec:
      {{- with .Values.image.pullSecrets }}
      imagePullSecrets:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      serviceAccountName: {{ include "auth.serviceAccount" . }}
      securityContext:
        {{- toYaml .Values.podSecurityContext | nindent 8 }}
      containers:
        - name: "auth"
          securityContext:
            {{- toYaml .Values.securityContext | nindent 12 }}
          image: "{{ .Values.image.repository }}:{{ .Values.image.tag | default .Chart.AppVersion }}"
          imagePullPolicy: {{ .Values.image.pullPolicy }}
          env:
            - name: RUST_LOG
              value: "info,auth=trace"
            - name: RUST_BACKTRACE
              value: "full"
            - name: ROCKET_ENV
              value: "production"
          ports:
            - name: http
              containerPort: 19253
              protocol: TCP
          livenessProbe:
            httpGet:
              path: /ping
              port: http
            periodSeconds: 10
            failureThreshold: 5
          readinessProbe:
            httpGet:
              path: /ping
              port: http
            periodSeconds: 10
            failureThreshold: 5
          volumeMounts:
            {{if .Values.persistence.enabled -}}
            - name: {{ include "auth.volumeClaim" . }}
              mountPath: /opt/veloren-auth/data
            {{ else }}
            - name: emptysaves
              mountPath: /opt/veloren-auth/data
            {{- end }}
          resources:
            {{- toYaml .Values.resources | nindent 12 }}
      {{if not .Values.persistence.enabled -}}
      volumes:
        - name: emptysaves
          emptyDir: {}
      {{- end }}
      {{- with .Values.nodeSelector }}
      nodeSelector:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      {{- with .Values.affinity }}
      affinity:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      {{- with .Values.tolerations }}
      tolerations:
        {{- toYaml . | nindent 8 }}
      {{- end }}
  {{if .Values.persistence.enabled -}}
  volumeClaimTemplates:
    - metadata:
        name: {{ include "auth.volumeClaim" . }}
        annotations:
        {{- with .Values.persistence.annotations }}
          {{- toYaml . | nindent 10 }}
        {{- end }}
      spec:
        accessModes: [ "ReadWriteOnce" ]
        storageClassName: {{ .Values.persistence.storageClass }}
        resources:
          requests:
            storage: {{ .Values.persistence.size }}
  {{- end }}