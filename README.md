[![pipeline status](https://gitlab.com/veloren/helm/badges/master/pipeline.svg)](https://gitlab.com/veloren/veloren/commits/master)
[![license](https://img.shields.io/github/license/veloren/helm.svg)](https://gitlab.com/veloren/helm/blob/master/LICENSE)
[![discord](https://img.shields.io/discord/449602562165833758.svg)](https://discord.gg/veloren-community-449602562165833758)

## Welcome to Veloren Helm Charts!

[Veloren](https://gitlab.com/veloren/veloren/) is a multiplayer voxel RPG written in Rust.

## Helm charts

This repo contains all helm charts from the veloren community. Please not that they are all early-alpha and not yet stable!

